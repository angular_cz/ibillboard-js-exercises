export function timeoutPromiseFactory(delay, conditionFunction) {

  const executor = function(resolve, reject) {
    setTimeout(function() {
      const value = conditionFunction();
      if (value) {
        resolve(value);
      } else {
        reject(value);
      }
    }, delay);
  };

  return new Promise(executor);
}
