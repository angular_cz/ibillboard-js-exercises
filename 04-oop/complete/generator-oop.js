export class Generator {
  constructor(initialNumber = 0) {
    this.value = initialNumber;
  }

  increase() {
    return this.value++;
  }
}

export let sequenceGenerator = new Generator(1);

