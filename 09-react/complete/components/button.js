import React from 'React';

 let Button = (props) => {

  let onClick = () => {
    props.onClick(props.value);
  };

  return <button type="button"
                 className="number-button"
                 value={props.value}
                 onClick={onClick}>
    {props.value}
  </button>
};

Button.propTypes = {
  value: React.PropTypes.number.isRequired,
  onClick: React.PropTypes.func.isRequired
};


export default Button;