import { Operation } from "./operations";

export class Calculator {
  private operations: Map< string, Operation> = new Map();

  operand = '';
  operation = '';

  result: string | null = null;

  addOperation(operation: Operation) {
    this.operations.set(operation.name, operation);
  };

  calculate(name: string, a: string, b:string) {
    if (!this.operations.has(name)) {
      throw new TypeError('Unknown operation: ' + name);
    }

    const operation = this.operations.get(name) as Operation;
    return operation.calculate(a, b);
  }

  equals() {
    if (this.result === null) {
      this.result = '0';
    }

    if (this.operation) {
      this.result = this.calculate(this.operation, this.result, this.operand).toString();
    }

    return this.result;
  }

  setOperand(operand: string) {
    this.operand = operand;
    if (this.result === null || !this.operation) {
      this.result = operand;
    }
  }

  setOperation(operation: string) {
    if (!this.operations.has(operation)) {
      throw new TypeError('Unknown operation: ' + operation);
    }

    this.operation = operation;
  }

  clear() {
    this.result = null;
    this.operand = '';
    this.operation = '';
  }
}
