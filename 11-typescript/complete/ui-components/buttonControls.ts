import { Calculator } from "../calculations/calculator";
import { Display } from "./display";

export class ButtonControls {
  constructor(private buttonsElement: HTMLElement,
              private calculator: Calculator,
              private display:Display) {

    buttonsElement.addEventListener("click", (event) => this.processButton(event));
  }

  processButton(event: MouseEvent) {

    const element = event.target as HTMLElement;

    if (element.tagName !== "BUTTON") {
      return;
    }
    const button = element as HTMLButtonElement;

    if (button.classList.contains("operation")) {
      this.processOperation_(button.name);
    } else {
      const value = button.getAttribute("value") || '';
      this.processNumber_(value);
    }
  }

  processOperation_(type: string) {

    switch (type) {
      case "clear":
        this.calculator.clear();
        this.display.clear();
        break;

      case "equals":
        const result = this.calculator.equals();
        this.display.setValue(result);
        break;

      default:
        this.display.setValue(this.display.getValue());
        this.calculator.setOperand(this.display.getValue());

        this.calculator.setOperation(type);
    }
  }

  processNumber_(value: string) {
    this.display.addToValue(value);
    this.calculator.setOperand(this.display.getValue());
  }
}
