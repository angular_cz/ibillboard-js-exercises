module.exports = function(config) {
  config.set({
    basePath: '../../',

    autoWatch: false,
    singleRun: true,

    plugins: [
      'karma-chrome-launcher',
      'karma-firefox-launcher',
      'karma-phantomjs-launcher',
      'karma-jasmine',
      'karma-story-reporter',
      'karma-sourcemap-loader',
      'karma-typescript',
    ],

    frameworks: ["jasmine", "karma-typescript"],
    files: [
      'bower_components/es6-promise-polyfill/promise.js',
      'bower_components/fetch/fetch.js',
      'scripts/tests/bind-polyfill.js',
      'scripts/tests/es6_error.js',
      'bower_components/core.js/client/core.js',
      {pattern: "11-typescript/complete/**/*.ts"},
    ],
    preprocessors: {
      "**/*.ts": ["karma-typescript"],
    },
    reporters: ["dots"],
    browsers: ["PhantomJS"],
    tsconfig: "./tsconfig.json"
  });
};

